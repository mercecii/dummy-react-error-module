"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = WarningNotice;

var _react = _interopRequireDefault(require("react"));

require("./warningNotice.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function WarningNotice(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "warning-notice"
  }, "This is Warning Notice");
}