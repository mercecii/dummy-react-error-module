"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ErrorWrapper;

var _react = _interopRequireDefault(require("react"));

require("./errorWrapper.css");

var _ = require("../..");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ErrorWrapper(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "error-wrapper"
  }, "This is ErrorWrapper", /*#__PURE__*/_react.default.createElement(_.ErrorNotice, null), /*#__PURE__*/_react.default.createElement(_.WarningNotice, null));
}