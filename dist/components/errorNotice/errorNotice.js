"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = ErrorNotice;

var _react = _interopRequireDefault(require("react"));

require("./errorNotice.css");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ErrorNotice(props) {
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "error-notice"
  }, "This is error Notice");
}