
const HtmlWebpackPlugin= require('html-webpack-plugin');
const path = require('path');
const {WebpackManifestPlugin} = require('webpack-manifest-plugin');

module.exports = {
    mode: "development",
    entry: {
        "bundle": path.resolve(__dirname, "src", "index.js")
    },
    experiments: {
        outputModule: true
    },
    output: {
        filename: 'index.js', 
        /* this will create container-module-bundle.js or whatever we write in entry object inside output.path */
        path: path.resolve(__dirname, 'dist'),
        chunkFilename: '[id].js',
        clean: true, /* removes/deletes any extra files and creates only necessary files */ 
        module: true /* This needs more research https://webpack.js.org/configuration/output/#outputmodule */
    },

    plugins: [
        new HtmlWebpackPlugin({
                template: path.resolve(__dirname, "public", "index.html"),
                templateParameters: {
                    "myTitle": "Error Module" /* Check this out in public/index.html */
                },
                favicon: "./public/favicon.ico",
                filename: "./index.html"
        }),
        new WebpackManifestPlugin ({
            fileName: './manifest.json'
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/, 
                use: [
                    { loader: 'style-loader' },
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true
                        }
                    }
                ]
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: [
                    //without additional ettings, this will reference .babelrc
                    {loader: 'babel-loader'}
                ]
            },
            {
                test: /\.svg$/i,
                use: [
                    {
                        loader: 'svg-url-loader',
                        options: {
                            encoding: false,
                        },
                    },
                ],
            },
            {
                test: /\.ico$/i,
                use: [
                    {
                        loader: 'svg-url-loader',
                        options: {
                            encoding: false,
                        },
                    },
                ],
            }
            
        ]
    },
    devServer: {
        // static: './dist',
        port: 9092,
        webSocketServer: false /* static heartbeatInterval = 1000; this error wa appearing */
    },
    devtool: "source-map"
    
};

