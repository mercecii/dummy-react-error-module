import React from 'react';
import './errorWrapper.css';
import { ErrorNotice, WarningNotice } from '../..';


export default function ErrorWrapper(props) {
    return (
        <div className="error-wrapper">
            This is ErrorWrapper
            <ErrorNotice />
            <WarningNotice />
        </div>
    );
}